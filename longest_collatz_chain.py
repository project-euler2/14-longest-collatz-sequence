def longest_chain(ceiling):
    longest = 0
    start_number = 0
    for i in range(1,ceiling):
        num = i
        elements = 1
        while num != 1:
            if (num % 2) == 0:
                num = num/2
            else:
                num = 3*num + 1
            elements += 1
        if elements > longest:
            longest = elements
            start_number = i
    return start_number
print(longest_chain(1000000))